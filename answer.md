## Теоретичні питання
**1. Опишите каким образом можно создать новый HTML на странице**

node.createElement(tag) - создает новый элемент с указанным тегом<br>
node.innerHTML - можно вставить HTML разметку в виде текстовой строки<br>
node.insertAdjacentHTML(from, textHTML) - можно вставить HTML разметку в виде текстовой строки<br>


**2. Опишите что означает первый параметр функции insertAdjacentHTML и опишите возможные варианты этого параметра**

node.insertAdjacentHTML(from, textHTML) - парсит textHTML как штмл, и вставляет относительно node в позицию from<br>
from:<br>
beforebegin - перед node<br>
afterbegin - в начале node<br>
beforeend - в конце node<br>
afterend - после node<br>


**3. Как можно удалить элемент со страницы?**

node.remove() - удалит node<br>
node.removeChild(elem) - удалит elem, если он дочерний node<br>

